package br.com.udemy.music.add.api.gateway.http;

import br.com.udemy.music.add.api.gateway.json.MusicJson;
import br.com.udemy.music.add.api.service.SaveMusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/v1")
public class MusicController {

    @Autowired
    private SaveMusicService musicService;

    @PostMapping("/customers/{uuid}/musics")
    public String create(@PathVariable("uuid") String uuid, @RequestBody MusicJson customerJson) throws ExecutionException, InterruptedException, JsonProcessingException {
        customerJson.setUuidCustomer(uuid);
        return musicService.execute(customerJson);
    }
}
