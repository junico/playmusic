package br.com.udemy.music.add.api.service;

import br.com.udemy.music.add.api.gateway.json.MusicJson;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
public class SaveMusicService {

    @Autowired
    private ReplyingKafkaTemplate<String, String, String> kafkaTemplate;

    @Value("${kafka.topic.request-topic}")
    private String requestTopic;

    @Value("${kafka.topic.requestreply-topic}")
    private String requestReplyTopic;

    public String execute(MusicJson musicJson) throws ExecutionException, InterruptedException, JsonProcessingException {

        //convertendo objeto para string
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(musicJson);

        //montando o produver que ira ser enviado para o kafka
        ProducerRecord<String, String> record = new ProducerRecord<>(requestTopic, jsonString);
        record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, requestReplyTopic.getBytes()));

        //envia para o kafka
        RequestReplyFuture<String, String, String> sendAndReceive = kafkaTemplate.sendAndReceive(record);

        //recebe retorno
        SendResult<String, String> sendResult = sendAndReceive.getSendFuture().get();sendResult.getProducerRecord().headers().forEach(header -> System.out.println(header.key() + ":" + header.toString()));

        ConsumerRecord<String, String> consumerRecord = sendAndReceive.get();

        MusicJson customerJsonReturn = mapper.readValue(consumerRecord.value(), MusicJson.class);

        return customerJsonReturn.getUuid();

    }
}
