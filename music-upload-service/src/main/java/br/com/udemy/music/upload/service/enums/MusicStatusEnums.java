package br.com.udemy.music.upload.service.enums;

public enum MusicStatusEnums {
    WAIT_SAVE_PATH,
    READY;
}
