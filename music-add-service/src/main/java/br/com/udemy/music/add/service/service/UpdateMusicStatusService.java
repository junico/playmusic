package br.com.udemy.music.add.service.service;

import br.com.udemy.music.add.api.gateway.json.MusicJson;
import br.com.udemy.music.add.service.domain.Music;
import br.com.udemy.music.add.service.enums.MusicStatusEnums;
import br.com.udemy.music.add.service.gateway.repository.MusicRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class UpdateMusicStatusService {

    @Autowired
    private MusicRepository musicRepository;

    @KafkaListener(topics = "${kafka.topic.music-status}", groupId = "${kafka.consumergroup}")
    public void execute(Object musicUpdate) throws JsonProcessingException {

        ConsumerRecord musicConsumer = (ConsumerRecord) musicUpdate;

        String json = (String) musicConsumer.value();

        ObjectMapper mapper = new ObjectMapper();

        MusicJson musicUpdateStatusJson = mapper.readValue(json, MusicJson.class);

        Optional<Music> music = musicRepository.findById(UUID.fromString(musicUpdateStatusJson.getUuid()));

        music.get().setStatus(MusicStatusEnums.READY.toString());
        music.get().setPath(musicUpdateStatusJson.getPath());
        music.get().setSize(musicUpdateStatusJson.getSize());

        musicRepository.save(music.get());
    }

}
