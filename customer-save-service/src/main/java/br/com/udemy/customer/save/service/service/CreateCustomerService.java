package br.com.udemy.customer.save.service.service;

import br.com.udemy.customer.save.service.domain.Customer;
import br.com.udemy.customer.save.service.gateway.http.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CreateCustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public UUID execute(Customer customer) {
        customer.setId(UUID.randomUUID());
        customerRepository.save(customer);
        return customer.getId();
    }

}
