package br.com.udemy.music.upload.api.service;

import br.com.udemy.music.upload.api.gateway.json.MusicUploadJson;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.utils.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@Service
public class UploadMusicService {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Value("${kafka.topic.request-topic}")
    private String requestTopic;

    @Value("${kafka.header.uuid-customer}")
    private String headerCustomer;

    @Value("${kafka.header.uuid-music}")
    private String headerMusic;

    public void execute(MusicUploadJson musicUploadJson) throws IOException {

        Bytes bytes = new org.apache.kafka.common.utils.Bytes(musicUploadJson.getFile().getBytes());

        ProducerRecord<Bytes, Bytes> producerRecord = new ProducerRecord(requestTopic, bytes, bytes);
        producerRecord.headers().add(new RecordHeader(headerCustomer, musicUploadJson.getUuid().getBytes()));
        producerRecord.headers().add(new RecordHeader(headerMusic, musicUploadJson.getUuidMusic().getBytes()));

        kafkaTemplate.send(producerRecord);
    }
}
