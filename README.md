# Como subir via docker

#### Executar docker compose
* docker-compose -f docker-kafka.yml up 
* docker-compose -f docker-cassandra.yml up


#### Executar cassandra
****Executar sempre que for verificar no banco os dados****
* docker exec -it "idContainerCassanda" /bin/bash
* cqlsh -u cassandra -p cassandra

****Executar com a keyspace desejada para consultar dados após criar keyspace***
* use customer_keyspace;

#### Executar para criar keyspace e tabela no banco para CUSTOMER
* CREATE KEYSPACE IF NOT EXISTS customer_keyspace 
WITH REPLICATION = {'class':'NetworkTopologyStrategy', 'datacenter1':3};

* use customer_keyspace;
* CREATE TABLE Customer(id UUID PRIMARY KEY, name text, country text, musicStyle text);

#### Executar para criar keyspace e tabela no banco para MUSIC
* CREATE KEYSPACE IF NOT EXISTS music_keyspace 
WITH REPLICATION = {'class':'NetworkTopologyStrategy', 'datacenter1':3};

* use music_keyspace;
* CREATE TABLE Music(id UUID PRIMARY KEY, name text, path text, status text, uuidCustomer text, size int);